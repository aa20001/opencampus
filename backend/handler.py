from abstract.hardware import Hardware
from abstract.plugin import Plugin
from dataclasses import dataclass


@dataclass
class CallReturn:
    """
    実行結果を格納するデータクラス

    is_error: 例外発生時にTrueになる
    result: 結果 なんでもいいがJSONにできる程度のものにしておくと良い
    """
    is_error: bool
    exec_info: dict
    result: any

    def to_dict(self):
        return {"is_error": self.is_error, "exec_info": self.exec_info, "result": self.result}


class Handler:
    def __init__(self, config_loader: 'ConfigLoader', hardware: 'Hardware', plugin: 'Plugin'):
        """
        処理実行部を構成するクラスの初期化
        :param hardware: その時のハードウェアクラス
        :param plugin: その時のプラグインクラス
        """
        self.hardware: Hardware = hardware
        self.plugin: Plugin = plugin
        self.config_loader: 'ConfigLoader' = config_loader

    def get_instance(self, target_type, target):
        if target_type == "hardware":
            instance = self.hardware.get_hardware(target)
        elif target_type == "plugin":
            instance = self.plugin.get_plugin(target)
        elif target_type == "system":
            instance = System(self)
        else:
            instance = None
        return instance

    def execute(self, exec_info) -> CallReturn:
        try:
            target_type = exec_info["target_type"]
            target = exec_info["target"]
            execute = exec_info["execute"]
            args = exec_info.get("args", [])
            kwargs = exec_info.get("kwargs", {})
        except KeyError as e:
            return CallReturn(True, exec_info, f"invalid exec_info: {e.args}")

        try:
            if not (instance := self.get_instance(target_type, target)):
                return CallReturn(True, exec_info, f"target_type {target_type} not found")
        except KeyError:
            return CallReturn(True, exec_info, f"target {target} not found")

        try:
            exec_func = getattr(instance, execute)
        except AttributeError:
            return CallReturn(True, exec_info, f"execute {execute} not found")

        if exec_func.__module__.startswith("abstract"):
            return CallReturn(True, exec_info, f"execute {execute} is parent class method")

        if execute.startswith("_"):
            return CallReturn(True, exec_info, f"private method execute is not allowed")

        if not callable(exec_func):
            return CallReturn(True, exec_info, f"execute {execute} is not callable")

        try:
            result = exec_func(*args, **kwargs)
            return CallReturn(False, exec_info, result)
        except Exception as e:
            return CallReturn(True, exec_info, f"{e.__class__.__name__}: {e.args}")


class System:
    def __init__(self, handler):
        self.handler = handler

    def get_config(self):
        return self.handler.config_loader.get_config()

    def get_functions(self, target_type, target):
        try:
            instance = self.handler.get_instance(target_type, target)
        except KeyError:
            return f"target {target} not found"
        if not instance:
            return f"target_type {target_type} not found"

        functions = []

        for dn in dir(instance):
            attr = getattr(instance, dn)
            if callable(attr) and not dn.startswith("_") and not attr.__module__.startswith("abstract"):
                functions.append(dn)

        return functions

    def get_plugins(self):
        return self.handler.plugin.get_plugin()

    def get_hardware(self):
        return self.handler.hardware.get_hardware()






