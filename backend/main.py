from abstract.plugin import Plugin
from abstract.hardware import Hardware
from handler import Handler
import importlib
from pathlib import Path
from config_loader import ConfigLoader
try:
    import tomllib
except ImportError:
    import toml as tomllib


if __name__ == '__main__':
    print(tomllib.load(Path("./config.toml").open()))
    loader = ConfigLoader("./config.toml")
    loader.load()
    loader.net.run()
    print(Hardware._Hardware__HARDWARE)
    print(Hardware.get_hardware("I2C").reset_hardware.__module__)


