from pathlib import Path
from abstract.hardware import Hardware
from abstract.plugin import Plugin
import importlib
import os
from handler import Handler
try:
    import tomllib
except ImportError:
    import toml as tomllib


class ConfigLoader:
    def __init__(self, config_path: os.PathLike):
        self.config_path = config_path
        self.hardware_modules = {}
        self.plugin_modules = {}
        self.net_modules = {}
        self.handler = None
        self.net = None

    def _import_hardware_module(self):
        for module_path in Path("./hardware/").glob("*.py"):
            module_name = f"hardware.{module_path.name.split('.')[0]}"
            self.hardware_modules[module_name] = importlib.import_module(module_name)

        for module_path in Path("./hardware/").glob("*/"):
            module_name = f"hardware.{module_path.name.split('.')[0]}"
            self.hardware_modules[module_name] = importlib.import_module(module_name)

    def _import_plugin_module(self):
        for module_path in Path("./plugins/").glob("*.py"):
            module_name = f"hardware.{module_path.name.split('.')[0]}"
            self.plugin_modules[module_name] = importlib.import_module(module_name)

        for module_path in Path("./plugins/").glob("*/"):
            module_name = f"plugins.{module_path.name.split('.')[0]}"
            self.plugin_modules[module_name] = importlib.import_module(module_name)

    def _import_net_module(self):
        for module_path in Path("./net/").glob("*.py"):
            module_name = f"net.{module_path.name.split('.')[0]}"
            print(module_name)
            self.net_modules[module_name] = importlib.import_module(module_name)

        for module_path in Path("./net/").glob("*/"):
            module_name = f"net.{module_path.name.split('.')[0]}"
            self.net_modules[module_name] = importlib.import_module(module_name)

    @staticmethod
    def _create_instance(class_, init=None):
        if init:
            return class_(*init.get("args", []), **init.get("kwargs", {}))
        else:
            return class_()
        pass

    @staticmethod
    def _get_module_name(base, path):
        return f"{base}." + '.'.join(path.split("/")[:-1])

    def get_config(self):
        return tomllib.load(Path(self.config_path).open())

    def _load(self):
        config = self.get_config()

        for hardware in config["hardware"]:
            module_name = "hardware." + '.'.join(hardware["class"].split(".")[:-1])
            class_name = hardware["class"].split(".")[-1]
            module = self.hardware_modules[module_name]
            class_ = getattr(module, class_name)
            self._create_instance(class_, hardware.get("init", None))

        for plugin in config["plugins"]:
            module_name = "plugins." + '.'.join(plugin["class"].split(".")[:-1])
            class_name = plugin["class"].split(".")[-1]
            module = self.plugin_modules[module_name]
            class_ = getattr(module, class_name)
            self._create_instance(class_, plugin.get("init", None))

        self.handler = Handler(self, Hardware, Plugin)

        module_name = "net." + '.'.join(config["net"]["class"].split(".")[:-1])
        class_name = config["net"]["class"].split(".")[-1]
        module = self.net_modules[module_name]
        class_ = getattr(module, class_name)
        init = config["net"].get("init", {"args": []})
        if "args" not in init:
            init["args"] = []
        init["args"].insert(0, self)
        self.net = self._create_instance(class_, init)

    def load(self):
        self._import_hardware_module()
        self._import_plugin_module()
        self._import_net_module()
        self._load()

    def reload(self):
        Hardware.reset_hardware()
        Plugin.reset_plugin()

        for key in self.hardware_modules.keys():
            self.hardware_modules[key] = importlib.reload(self.hardware_modules[key])

        for key in self.plugin_modules.keys():
            self.plugin_modules[key] = importlib.reload(self.plugin_modules[key])

        for key in self.net_modules.keys():
            self.net_modules[key] = importlib.reload(self.net_modules[key])

        self._load()

