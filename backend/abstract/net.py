from config_loader import ConfigLoader
from abc import abstractmethod


class Net:
    """
    ネット用の抽象レイヤ
    このクラスを継承したクラスの初期化引数の第一引数はConfigLoaderである必要がある
    """

    def __init__(self, config_loader: ConfigLoader):
        self.config_loader = config_loader

    @abstractmethod
    def run(self):
        pass

