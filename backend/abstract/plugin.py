from __future__ import annotations

from typing import Optional

from abstract.hardware import Hardware


class Plugin:
    __PLUGINS: dict[str, Plugin] = {}

    def __init__(self, name: str, description: str, used_hardware: list[str] = None):
        if used_hardware is None:
            used_hardware = []

        if name in Plugin.__PLUGINS:
            raise SyntaxError(f"Plugin {name} are already exists")
        for uh in used_hardware:
            # ハードウェアが利用可能か確認し利用できる場合は登録する
            Hardware.use_hardware(uh)

        Plugin.__PLUGINS[name] = self
        self.__name = name
        self.__description = description

    @classmethod
    def get_plugin(cls, plugin_identity: Optional[str] = None):
        if plugin_identity:
            return cls.__PLUGINS[plugin_identity]
        return list(cls.__PLUGINS.keys())

    @classmethod
    def reset_plugin(cls):
        cls.__PLUGINS.clear()

    @property
    def name(self):
        return self.__name

    @property
    def description(self):
        return self.__description


