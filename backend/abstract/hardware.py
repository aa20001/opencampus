from __future__ import annotations

from abc import abstractmethod
from typing import Optional


class Hardware:
    __HARDWARE: dict[str, Hardware] = {}

    def __init__(self, name: str, description: str, used_hardware: list[str] = None):
        """
        Hardware抽象クラスの初期化
        子クラスの初期化時に同じ条件のものを登録できないように実装する必要があるかも

        :param name: ハードウェアの名前 これは全てのハードウェアに対してユニークである必要があある
        :param used_hardware: ハードウェアを使うために使うハードウェアの一覧
        [GPIO.10, I2C, S2P.any_identity] のような形式で記述する
        :param description: ハードウェアの説明
        """
        if used_hardware is None:
            used_hardware = []

        if name in Hardware.__HARDWARE:
            raise SyntaxError(f"hardware {name} are already exists")
        for uh in used_hardware:
            # ハードウェアが利用可能か確認し利用できる場合は登録する
            Hardware.use_hardware(uh)

        Hardware.__HARDWARE[name] = self
        self.__name = name
        self.__description = description

    @classmethod
    def reset_hardware(cls):
        cls.__HARDWARE.clear()

    @classmethod
    def get_hardware(cls, hardware_id: Optional[str] = None):
        if hardware_id:
            return cls.__HARDWARE[hardware_id]
        return list(cls.__HARDWARE.keys())

    @classmethod
    def use_hardware(cls, hardware_identity: str) -> None:
        """
        指定されたハードウェアが利用可能か(重複する利用を許すかはインスタンスに任せる)確認し利用することを登録する
        :param hardware_identity: 使うハードウェアの識別子
        GPIO.10, I2C, S2P.any_identity のような形式
        :raise SyntaxError: 利用不可だった場合に発生
        """
        # あるハードウェアがハードウェアを使う場合そのハードウェアを重複して利用していないか確認する
        if hardware_identity.split(".")[0] not in Hardware.__HARDWARE:
            # ハードウェアが見つからない場合はエラーを発生される
            raise SyntaxError(f"hardware {hardware_identity.split('.')[0]} not found")
        # used_hardwareは.区切り形式で記述される 既に初期化済みのハードウェアに対して問い合わせと登録を実施する
        # ハードウェアidは.で区切った最初の部分
        hardware = Hardware.__HARDWARE[hardware_identity.split(".")[0]]
        internal_identity = ".".join(hardware_identity.split(".")[1:])
        if hardware._is_conflict(internal_identity):
            # もし競合していたらエラーを発生させる
            raise SyntaxError(f"hardware {hardware_identity} are already used")
        else:
            # 競合していないなら登録する
            hardware._use_hardware(internal_identity)

    @abstractmethod
    def _is_conflict(self, internal_identity: str) -> bool:
        """
        ハードウェアを利用可能か確認する
        :param internal_identity: 内部識別子　GPIO.19の場合は19が内部識別子となる
        :return: cant_use: 利用不可能な場合はTrueを返す
        """

    @abstractmethod
    def _use_hardware(self, internal_identity: str) -> None:
        """
        ハードウェアを利用することを登録する
        :param internal_identity: 内部識別子　GPIO.19の場合は19が内部識別子となる
        :return: None
        """

    @property
    def name(self):
        return self.__name

    @property
    def description(self):
        return self.__description
