
Project Directory
|
|━ hardware
|         |━ 温度
|         |      |━ __init__.py
|         |
|         |━ 湿度
|         |      |━ __init__.py
|         |
|         |━ 気圧.py
|
|━ plugins
|        |━ plugin1
|
|━ net
|       |━ scaledrone.py
|
|━ abstruct
|       |━ main.py
|       |━ plugin.py
|       |━ hardware.py
|
|━ handler.py
|━ main.py
|━ config.toml
|━ requirements.txt

hardware ハードウェア関連のパッケージ等を設置する
plugins ハードウェアを使ったプログラムによる制御等のためのパッケージを設置する
net 通信をうまく別のプロトコルでもうまく動かすためのパッケージ等を設置する
config.toml ハードウェアの設定や通信方式の設定を書く
main.py 実行ファイル本体
requirements.txt プログラムを使用するために必要なモジュール等を書く
handler.py 呼び出し先の管理用
interface id等を管理するための抽象クラスを置く
