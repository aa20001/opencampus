connect wss://api.scaledrone.com/v3/websocket

{"type": "handshake", "channel": "8Ep3TIYjtaDhge2j", "callback": 0}
{"type": "subscribe", "room": "control", "callback": 1}
{"type": "subscribe", "room": "sync", "callback": 2}
{"type": "publish", "room": "control", "message": {"target_type": "hardware", "target": "temperture1", "execute": "get_temp"}}

{"type": "publish", "room": "control", "message": {"target_type": "system", "target": "", "execute": "get_plugins"}}
{"type": "publish", "room": "control", "message": {"target_type": "system", "target": "", "execute": "get_hardware"}}
{"type": "publish", "room": "control", "message": {"target_type": "system", "target": "", "execute": "get_config"}}
{"type": "publish", "room": "control", "message": {"target_type": "system", "target": "", "execute": "get_functions", "kwargs": {"target_type": "hardware", "target": "temperture1"}}}
