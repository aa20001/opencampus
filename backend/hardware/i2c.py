from abstract.hardware import Hardware


class I2C(Hardware):
    def __init__(self):
        super().__init__("I2C", "I2C　実際には何もしない", [])

    def _use_hardware(self, internal_identity: str):
        pass

    def _is_conflict(self, internal_identity: str):
        return False
