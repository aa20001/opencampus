from abstract.hardware import Hardware


class TempSensor(Hardware):
    _USED = {}

    def __init__(self, name: str, idx: str):
        super().__init__(name, "temperature sensor for test", ["I2C"])
        if idx in self._USED:
            raise SyntaxError(f"index {idx} are already used")
        self.idx = idx

    def _use_hardware(self, internal_identity: str):
        pass

    def _is_conflict(self, internal_identity: str):
        return False

    def get_temp(self):
        if self.idx == "0":
            return 20
        elif self.idx == "1":
            return 30
        return 40

