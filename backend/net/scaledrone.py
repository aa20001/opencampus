from abstract.net import Net
import asyncio
import websockets
import json


message_id = 0
def get_message_id():
    global message_id
    message_id += 1
    return message_id-1

HANDSHAKE_DATA = {
  "type": "handshake",
  "channel": "8Ep3TIYjtaDhge2j",
  "callback": get_message_id(),
  "client_data": {"type": "service", "id": 0}
}

SUBSCRIBE_DATA = {
  "type": "subscribe",
  "room": "control",
  "callback": get_message_id()
}


class ScaleDrone(Net):
    def __init__(self, config_loader, channel: str):
        self.channel = channel
        super().__init__(config_loader)

    def run(self):
        asyncio.run(self.main())

    async def main(self):
        while True:
            try:
                async with websockets.connect("wss://api.scaledrone.com/v3/websocket") as websocket:
                    await websocket.send(json.dumps(HANDSHAKE_DATA))
                    res = await websocket.recv()
                    if "error" in json.loads(res):
                        print(f"join error: {res}")
                        return

                    await websocket.send(json.dumps(SUBSCRIBE_DATA))
                    res = await websocket.recv()
                    if "error" in json.loads(res):
                        print(f"join error: {res}")
                        return
                    print("connected")

                    while True:
                        msg = await websocket.recv()
                        try:
                            msg = json.loads(msg)
                        except:
                            continue

                        if msg["type"] != "publish":
                            continue

                        if not isinstance(msg["message"], dict):
                            continue

                        message = msg["message"]
                        result = self.config_loader.handler.execute(message)
                        await websocket.send(
                                json.dumps({"type": "publish", "room": "sync", "message": result.to_dict()}))
            except websockets.exceptions.ConnectionClosedError:
                continue
